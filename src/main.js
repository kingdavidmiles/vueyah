import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import { auth } from "./firebase";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import { NavbarPlugin } from "bootstrap-vue";
import VueCarousel from "vue-carousel";
import VueShareSocial from "vue-share-social";
import VueSocialSharing from "vue-social-sharing";
import AOS from "aos";
import "aos/dist/aos.css";
import VueSnip from "vue-snip";

Vue.use(VueSnip);
Vue.use(VueSocialSharing);
Vue.use(VueShareSocial);
Vue.use(VueCarousel);
Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(NavbarPlugin);
Vue.use(IconsPlugin);

let app;
auth.onAuthStateChanged((user) => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: (h) => h(App),
      mounted() {
        AOS.init();
      },
    }).$mount("#app");
  }

  if (user) {
    store.dispatch("fetchUserProfile", user);
  }
});
