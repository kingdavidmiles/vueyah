import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
// AUTHENTICATION
import Register from "../views/auth/Register";
import Login from "../views/auth/Login";
import PasswordReset from "../views/auth/PasswordReset";
import UserProfile from "../views/auth/UserProfile";

// POST
import CreatePost from "../views/post/create-post";
import GetALlPosts from "../views/post/getAll-posts";
import GetALlPost from "../views/post/_getPost";

// PASTOR
import Pastors from "../views/Pastors.vue";

// Calender
import Calender from "../views/Calender.vue";

// Yahrzeit;
import Yahrzeit from "../views/yahrzeit.vue";

// Paraschat
import Paraschat from "../views/Paraschat.vue";

// Testimones
import CreateTestimoney from "../views/Testimonies/CreateTestimoney";
import GetAllTestimonies from "../views/Testimonies/GetAllTestimonies";
import GetATestimony from "../views/Testimonies/_getTestimony";

// YOUTHS
import CreateYouthHookup from "../views/youth/CreateYouthHookup";
import GetAllYouthMeetUp from "../views/youth/GetAllYouthMeetUp";

//  DONATIONS
import FeedThePeople from "../views/DonationList/FeedThePeople.vue";
import churchRenovation from "../views/DonationList/ChurchRenovation.vue";
import Donation from "../views/DonationList/index.vue";
// FIREBASE
import { auth } from "../firebase";
import "firebase/auth";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/About.vue"),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/pastor",
    name: "pastor",
    component: Pastors,
  },
  {
    path: "/calender",
    name: "Calender",
    component: Calender,
  },
  // {
  //   path: "/siddur",
  //   name: "siddur",
  //   component: Siddur,
  // },
  {
    path: "/yahrzeit",
    name: "yahrzeit",
    component: Yahrzeit,
  },

  {
    path: "/paraschat",
    name: "Paraschat",
    component: Paraschat,
  },
  //      _         _   _                _   _           _   _
  //     / \  _   _| |_| |__   ___ _ __ | |_(_) ___ __ _| |_(_) ___  _ __       _______  _ __   ___
  //    / _ \| | | | __| '_ \ / _ \ '_ \| __| |/ __/ _` | __| |/ _ \| '_ \     |_  / _ \| '_ \ / _ \
  //   / ___ \ |_| | |_| | | |  __/ | | | |_| | (_| (_| | |_| | (_) | | | |     / / (_) | | | |  __/
  //  /_/   \_\__,_|\__|_| |_|\___|_| |_|\__|_|\___\__,_|\__|_|\___/|_| |_|    /___\___/|_| |_|\___|

  {
    path: "/register",
    name: "Register",
    component: Register,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },

  {
    path: "/password-reset",
    name: "PasswordReset",
    component: PasswordReset,
  },
  {
    path: "/profile",
    name: "UserProfile",
    component: UserProfile,
    meta: {
      requiresAuth: true,
    },
  },

  //   ____                          _
  //  |  _ \       ___      ___     | |_      ___
  //  | |_) |     / _ \    / __|    | __|    / __|
  //  |  __/     | (_) |   \__ \    | |_     \__ \
  //  |_|         \___/    |___/     \__|    |___/

  {
    path: "/create-post",
    name: "CreatePost",
    component: CreatePost,
    meta: {
      requiresAuth: true,
    },
  },

  {
    path: "/blog",
    name: "GetALlPosts",
    component: GetALlPosts,
  },
  {
    name: "get-post",
    path: "/getpost/:id",
    component: GetALlPost,
  },

  //   _____                _     _                               _
  //  |_   _|   ___   ___  | |_  (_)  _ __ ___     ___    _ __   (_)   ___   ___
  //    | |    / _ \ / __| | __| | | | '_ ` _ \   / _ \  | '_ \  | |  / _ \ / __|
  //    | |   |  __/ \__ \ | |_  | | | | | | | | | (_) | | | | | | | |  __/ \__ \
  //    |_|    \___| |___/  \__| |_| |_| |_| |_|  \___/  |_| |_| |_|  \___| |___/

  {
    name: "createTestimoney",
    path: "/create_testimoney",
    component: CreateTestimoney,
    meta: {
      requiresAuth: true,
    },
  },

  {
    name: "getAllTestimonies",
    path: "/get_testimonies",
    component: GetAllTestimonies,
  },

  {
    name: "getTestimoney",
    path: "/getTestimony/:id",
    component: GetATestimony,
  },

  // YOUTH

  {
    name: "CreateYouthHookup",
    path: "/create-youth",
    component: CreateYouthHookup,
  },
  {
    name: "youth-meetup",
    path: "/youth-meetup",
    component: GetAllYouthMeetUp,
  },

  // DONATIONS
  {
    name: "Donation",
    path: "/donation_list",
    component: Donation,
  },
  // {
  //   name: "HelpTheChildren",
  //   path: "/donation/help_children",
  //   component: HelpTheChildren,
  // },
  {
    name: "FeedThePeople",
    path: "/donation/feed_the_people",
    component: FeedThePeople,
  },
  {
    name: "churchRenovation",
    path: "/donation/church_renovation",
    component: churchRenovation,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some((x) => x.meta.requiresAuth);

  if (requiresAuth && !auth.currentUser) {
    next("/login");
  } else {
    next();
  }
});
export default router;
