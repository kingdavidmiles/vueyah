// @ts-check
import Vue from "vue";
import Vuex from "vuex";
import * as fb from "../firebase";
import router from "../router/index";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    userProfile: {},
    posts: [],
    testmony: [],
  },
  mutations: {
    setUserProfile(state, val) {
      state.userProfile = val;
    },
    setPost(state, val) {
      state.posts = val;
    },

    setTestimoney(state, val) {
      state.testmony = val;
    },
  },
  actions: {
    //       _                                        _
    //   ___(_) __ _ _ __     _   _ ___  ___ _ __    (_)_ __
    //  / __| |/ _` | '_ \   | | | / __|/ _ \ '__|   | | '_ \
    //  \__ \ | (_| | | | |  | |_| \__ \  __/ |      | | | | |
    //  |___/_|\__, |_| |_|   \__,_|___/\___|_|      |_|_| |_|
    //         |___/
    async login({ dispatch }, form) {
      try {
        const { user } = await fb.auth.signInWithEmailAndPassword(form.email, form.password);
        dispatch("fetchUserProfile", user);
      } catch (error) {
        window.alert(error);
      }

      // fetch user profile and set in state
    },

    //       _
    //   ___(_) __ _ _ __      _   _ ___  ___ _ __     _   _ _ __
    //  / __| |/ _` | '_ \    | | | / __|/ _ \ '__|   | | | | '_ \
    //  \__ \ | (_| | | | |   | |_| \__ \  __/ |      | |_| | |_) |
    //  |___/_|\__, |_| |_|    \__,_|___/\___|_|       \__,_| .__/
    //         |___/                                        |_|

    async signup({ dispatch }, form) {
      console.log(form);

      const { user } = await fb.auth.createUserWithEmailAndPassword(form.email, form.password);

      // create user object in userCollections
      await fb.usersCollection.doc(user.uid).set({
        name: form.name,
        // about: form.about,
        email: form.email,
        facebook: form.facebook,
        twitter: form.twitter,
        password: form.password,
        date: new Date(),
      });

      // fetch user profile and set in state
      dispatch("fetchUserProfile", user);
      router.push("/");
    },

    //   __      _       _                                                __ _ _
    //  / _| ___| |_ ___| |__     _   _ ___  ___ _ __    _ __  _ __ ___  / _(_) | ___
    // | |_ / _ \ __/ __| '_ \   | | | / __|/ _ \ '__|  | '_ \| '__/ _ \| |_| | |/ _ \
    // |  _|  __/ || (__| | | |  | |_| \__ \  __/ |     | |_) | | | (_) |  _| | |  __/
    // |_|  \___|\__\___|_| |_|   \__,_|___/\___|_|     | .__/|_|  \___/|_| |_|_|\___|
    //                                                  |_|

    async fetchUserProfile({ commit }, user) {
      const userProfile = await fb.usersCollection.doc(user.uid).get();

      // set user profile in state
      commit("setUserProfile", userProfile.data());

      // change route to dashboard
      if (router.currentRoute.path === "/login") {
        router.push("/");
      }
    },

    //   _                               _
    //  | |    ___   __ _     ___  _   _| |_
    //  | |   / _ \ / _` |   / _ \| | | | __|
    //  | |__| (_) | (_| |  | (_) | |_| | |_
    //  |_____\___/ \__, |   \___/ \__,_|\__|
    //              |___/

    async logout({ commit }) {
      // log user out
      await fb.auth.signOut();

      // clear user data from state
      commit("setUserProfile", {});

      // redirect to login view
      router.push("/login");
    },

    //                   _       _                                                     __ _ _
    //   _   _ _ __   __| | __ _| |_ ___      _   _ ___  ___ _ __     _ __  _ __ ___  / _(_) | ___
    //  | | | | '_ \ / _` |/ _` | __/ _ \    | | | / __|/ _ \ '__|   | '_ \| '__/ _ \| |_| | |/ _ \
    //  | |_| | |_) | (_| | (_| | ||  __/    | |_| \__ \  __/ |      | |_) | | | (_) |  _| | |  __/
    //   \__,_| .__/ \__,_|\__,_|\__\___|     \__,_|___/\___|_|      | .__/|_|  \___/|_| |_|_|\___|
    //        |_|                                                    |_|

    async updateProfile({ dispatch }, user) {
      const userId = fb.auth.currentUser.uid;
      // update user object
      const userRef = await fb.usersCollection.doc(userId).update({
        name: user.name,
        title: user.title,
        facebook: user.facebook,
        twitter: user.twitter,
      });

      dispatch("fetchUserProfile", { uid: userId });

      // update all posts by user
      const postDocs = await fb.postsCollection.where("userId", "==", userId).get();
      postDocs.forEach((doc) => {
        fb.postsCollection.doc(doc.id).update({
          userName: user.name,
          facebook: user.facebook,
          twitter: user.twitter,
        });
      });
    },
    //    ____                _                            _
    //   / ___|_ __ ___  __ _| |_ ___      _ __   ___  ___| |_
    //  | |   | '__/ _ \/ _` | __/ _ \    | '_ \ / _ \/ __| __|
    //  | |___| | |  __/ (_| | ||  __/    | |_) | (_) \__ \ |_
    //   \____|_|  \___|\__,_|\__\___|    | .__/ \___/|___/\__|
    //                                    |_|

    async createPost({ state, commit }, posts) {
      // create posts in firebase
      await fb.postsCollection.add({
        createdOn: new Date(),
        title: posts.title,
        content: posts.content,
        userId: fb.auth.currentUser.uid,
        userName: state.userProfile.name,

        likesCount: 0,
      });

      router.push("/get-posts");
      // commit("setPost");
    },

    async getPosts() {
      await fb.postsCollection
        .orderBy("createdOn", "desc")
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
          });
        });
    },

    //   _       _   _                                     _
    //  | |     (_) | | __   ___     _ __     ___    ___  | |_
    //  | |     | | | |/ /  / _ \   | '_ \   / _ \  / __| | __|
    //  | |___  | | |   <  |  __/   | |_) | | (_) | \__ \ | |_
    //  |_____| |_| |_|\_\  \___|   | .__/   \___/  |___/  \__|
    //                              |_|

    async likePost({ commit }, payload) {
      const { postId } = payload;

      const userId = fb.auth.currentUser.uid;

      // check if user has liked post
      const likesRef = await fb.likesCollection.doc(userId).get();
      const prevPostRef = await fb.postsCollection.doc(postId).get();

      if (likesRef.exists) {
        return;
      }

      // create like
      // await fb.likesCollection.doc(userId).set({
      //   postId,
      //   userId,
      // });

      let newLocal = prevPostRef.data().likesCount ?? 0;
      // @ts-ignore
      const likesCount = (newLocal += 1);

      // if (newLocal > 2) {
      //   return 1;
      // }
      // console.log(
      //   likesCount,
      //   Number.isNaN(prevPostRef.data().likesCount),
      //   prevPostRef.data().likesCount
      // );
      // update post likes count
      await fb.postsCollection.doc(postId).update({
        likesCount,
      });
    },

    //   _____                _     _                               _
    //  |_   _|   ___   ___  | |_  (_)  _ __ ___     ___    _ __   (_)   ___   ___
    //    | |    / _ \ / __| | __| | | | '_ ` _ \   / _ \  | '_ \  | |  / _ \ / __|
    //    | |   |  __/ \__ \ | |_  | | | | | | | | | (_) | | | | | | | |  __/ \__ \
    //    |_|    \___| |___/  \__| |_| |_| |_| |_|  \___/  |_| |_| |_|  \___| |___/

    async createTestimoney({ state, commit }, testmony) {
      // create testmony in firebase
      await fb.testmonysCollection.add({
        createdOn: new Date(),
        title: testmony.title,
        content: testmony.content,
        userId: fb.auth.currentUser.uid,
        userName: state.userProfile.name,
      });

      router.push("/get_testimonies");
      // commit("setPost");
    },

    async getTestimonies() {
      await fb.testmonysCollection
        .orderBy("createdOn", "desc")
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
          });
        });
    },
  },
});

export default store;
