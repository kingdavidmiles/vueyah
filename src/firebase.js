import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

// firebase init
const firebaseConfig = {
  apiKey: "AIzaSyDDJ7blURdtMz-v7L8xo9FGGcgw43IQe6U",
  authDomain: "vuefire-3ccc4.firebaseapp.com",
  projectId: "vuefire-3ccc4",
  storageBucket: "vuefire-3ccc4.appspot.com",
  messagingSenderId: "675704521349",
  appId: "1:675704521349:web:36405bbaa9085e78c89fec",
  measurementId: "G-FFGE0P4W4K",
};
firebase.initializeApp(firebaseConfig);

// utils
const db = firebase.firestore();
const auth = firebase.auth();

// collection references
const usersCollection = db.collection("users");
const postsCollection = db.collection("posts");
const testmonysCollection = db.collection("testimonies");
const commentsCollection = db.collection("comments");
const likesCollection = db.collection("likes");

// export utils/refs
export {
  db,
  auth,
  usersCollection,
  postsCollection,
  commentsCollection,
  likesCollection,
  testmonysCollection,
};
